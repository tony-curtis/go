#!/bin/sh -e

prog=$1
shift

if [ -r ./setenv.sh ]
then
    source ./setenv.sh
fi

go install shmem

go build $prog

oshrun $@
