package main

import (
    "fmt"
    "bitbucket.org/tony-curtis/go/shmem"
)

// symmetric
var dest int = 9

func main() {
    me := shmem.MyPe()

    if me == 0 {
        shmem.IntAtomicAdd(&dest, 21, 1)
    }

    shmem.BarrierAll()

    fmt.Printf("%-4d dest = %-4d\n", me, dest)

    shmem.Finalize()
}
