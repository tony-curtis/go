package shmem

// #include <shmem.h>
import "C"

import (
    "unsafe"
    "fmt"
    "reflect"
)

//
// We'll have to define these ourselves for now.  If these are
// #defines, then there's no symbol to find.  This is not optimal -
// easy to break things
//

const BCAST_SYNC_SIZE         = 64
const BARRIER_SYNC_SIZE       = 2
const REDUCE_SYNC_SIZE        = 64
const REDUCE_MIN_WRKDATA_SIZE = 64
const SYNC_VALUE              = 0

const MAX_NAME_LEN            = 64

const (
    THREAD_SINGLE = iota
    THREAD_FUNNELED
    THREAD_SERIALIZED
    THREAD_MULTIPLE
)

const (
    CMP_EQ = iota
    CMP_NE
    CMP_GT
    CMP_LE
    CMP_LT
    CMP_GE
)

type Lock = int64

const LOCK_INIT = 0

type Memory = unsafe.Pointer

//
// Start and stop.  N.B. the latest version of Go! exits via
// exit_group() so never gets to atexit() handlers.  An explicit
// Finalize() is required.
//

func init() {
    C.shmem_init()
}

func Init() {
    C.shmem_init()
}
func StartPes(n int) {
    C.shmem_init()
}
func Finalize() {
    C.shmem_finalize()
}

func GlobalExit(status int) {
    C.shmem_global_exit(C.int(status))
}

//
// rank query
//

func MyPe() int {
    return int(C.shmem_my_pe())
}

func NPes() int {
    return int(C.shmem_n_pes())
}

//
// version query
//

func InfoGetVersion() (int, int) {
    var maj C.int
    var min C.int
    C.shmem_info_get_version(&maj, &min)
    return int(maj), int(min)
}

func InfoGetName() string {
    var s [MAX_NAME_LEN]C.char
    sp := &s[0]
    C.shmem_info_get_name(sp)
    return C.GoString(sp)
}

//
// ordering and completion
//

func Quiet() {
    C.shmem_quiet()
}

func Fence() {
    C.shmem_fence()
}

//
// address accessibility
//

func Ptr(a Memory, pe int) Memory {
    return C.shmem_ptr(a, C.int(pe))
}

func PeAccessible(pe int) bool {
    return C.shmem_pe_accessible(C.int(pe)) == 1
}

func AddrAccessible(a Memory, pe int) bool {
    return C.shmem_addr_accessible(a, C.int(pe)) == 1
}

//
// collectives
//

func BarrierAll() {
    C.shmem_barrier_all()
}

func Barrier(pe_start int, log_stride int, pe_size int, psync *int64) {
    ps := (*C.long)(psync)
    C.shmem_barrier(C.int(pe_start), C.int(log_stride), C.int(pe_size), ps)
}

func Broadcast32(dst Memory, src Memory,
    nelems uint, pe_root int, pe_start int, log_stride int, pe_size int,
    psync *int64) {
    ps := (*C.long)(psync)
    C.shmem_broadcast32(dst, src,
        C.size_t(nelems), C.int(pe_root), C.int(pe_start),
        C.int(log_stride), C.int(pe_size), ps)
}

func Broadcast64(dst Memory, src Memory,
    nelems uint, pe_root int, pe_start int, log_stride int, pe_size int,
    psync *int64) {
    ps := (*C.long)(psync)
    C.shmem_broadcast64(dst, src,
        C.size_t(nelems), C.int(pe_root), C.int(pe_start),
        C.int(log_stride), C.int(pe_size), ps)
}

func DoubleSumToAll(dst *float64, src *float64,
    nreduce int, pe_start int, log_stride int, pe_size int,
    pwrk *float64, psync *int64) {
    d  := (*C.double)(dst)
    s  := (*C.double)(src)
    pw := (*C.double)(pwrk)
    ps := (*C.long)(psync)
    C.shmem_double_sum_to_all(d, s,
        C.int(nreduce),
        C.int(pe_start), C.int(log_stride), C.int(pe_size), pw, ps)
}

//
// RDMA (and so on for all the types)
//

func Put (dst interface{}, src interface{}, n uint, pe int) {
    vd := reflect.ValueOf(dst).Elem()
    vs := reflect.ValueOf(src).Elem()
    switch dst.(type) {
    case *int:
        pd := vd.Addr().Interface().(*int)
        ps := vs.Addr().Interface().(*int)
        IntPut(pd, ps, n, pe)
    case *float64:
        pd := vd.Addr().Interface().(*float64)
        ps := vs.Addr().Interface().(*float64)
        DoublePut(pd, ps, n, pe)
    default:
        fmt.Println("WHAT THE HELL?")
    }
}

func IntPut(dst *int, src *int, n uint, pe int) {
    d := (*C.int)(Memory(dst))
    s := (*C.int)(Memory(src))
    nbytes := n * uint(unsafe.Sizeof(*dst))
    C.shmem_int_put(d, s, C.size_t(nbytes), C.int(pe))
}

func DoublePut(dst *float64, src *float64, n uint, pe int) {
    d := (*C.double)(Memory(dst))
    s := (*C.double)(Memory(src))
    nbytes := n * uint(unsafe.Sizeof(*dst))
    C.shmem_double_put(d, s, C.size_t(nbytes), C.int(pe))
}

func FloatPut(dst *float32, src *float32, n uint, pe int) {
    d := (*C.float)(Memory(dst))
    s := (*C.float)(Memory(src))
    nbytes := n * uint(unsafe.Sizeof(*dst))
    C.shmem_float_put(d, s, C.size_t(nbytes), C.int(pe))
}

func IntP(dst *int, val int, pe int) {
    d := (*C.int)(Memory(dst))
    C.shmem_int_p(d, C.int(val), C.int(pe))
}

func IntG(src *int, pe int) int {
    s := (*C.int)(Memory(src))
    ret := C.shmem_int_g(s, C.int(pe))
    return int(ret)
}

//
// atomics
//

func IntAtomicFetchInc(dst *int, pe int) int {
    d := (*C.int)(Memory(dst))
    ret := C.shmem_int_atomic_fetch_inc(d, C.int(pe))
    return int(ret)
}

func IntAtomicInc(dst *int, pe int) {
    d := (*C.int)(Memory(dst))
    C.shmem_int_atomic_inc(d, C.int(pe))
}

func IntAtomicFetchAdd(dst *int, val int, pe int) int {
    d := (*C.int)(Memory(dst))
    ret := C.shmem_int_atomic_fetch_add(d, C.int(val), C.int(pe))
    return int(ret)
}

func IntAtomicAdd(dst *int, n int, pe int) {
    d := (*C.int)(Memory(dst))
    C.shmem_int_atomic_add(d, C.int(n), C.int(pe))
}

//
// locks
//

func SetLock(L *Lock) {
    ll := (*C.long)(L)
    C.shmem_set_lock(ll)
}

func ClearLock(L *Lock) {
    ll := (*C.long)(L)
    C.shmem_clear_lock(ll)
}

func TestLock(L *Lock) bool {
    ll := (*C.long)(L)
    return C.shmem_test_lock(ll) == 1
}

//
// memory allocation
//

func Malloc(nbytes uint) Memory {
    return C.shmem_malloc(C.size_t(nbytes))
}

func Calloc(nmem uint, size uint) Memory {
    return C.shmem_calloc(C.size_t(nmem), C.size_t(size))
}

func Free(m Memory) {
    C.shmem_free(m)
}

//
// wait and test
//

func LongWaitUntil(ivar *int64, cmp int, cmp_value int64) {
    ii := (*C.long)(ivar)
    C.shmem_long_wait_until(ii, C.int(cmp), C.long(cmp_value))
}

func LongTest(ivar *int64, cmp int, cmp_value int64) {
    ii := (*C.long)(ivar)
    C.shmem_long_test(ii, C.int(cmp), C.long(cmp_value))
}

//
// deprecated cache routines
//

func ClearCacheInv() {}
func SetCacheInv() {}
func ClearCachelineInv(dest Memory) {}
func SetCacheLineInv(dest Memory) {}
func Udcflush() {}
func UdcflushLine(dest Memory) {}
