package main

import (
    "fmt"
    "time"
    "bitbucket.org/tony-curtis/go/shmem"
)

var L shmem.Lock = shmem.LOCK_INIT

func main() {

    const slp = time.Second

    me := shmem.MyPe()

    if me == 1 {
        time.Sleep(5 * time.Second)
    }

    shmem.SetLock(&L)

    fmt.Printf("%d: sleeping %d seconds...\n", me, slp / time.Second)
    time.Sleep(slp)
    fmt.Printf("%d: sleeping...done\n", me)

    shmem.ClearLock(&L)
}
