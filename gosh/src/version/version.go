package main

import (
    "fmt"
    "bitbucket.org/tony-curtis/go/shmem"   // OpenSHMEM namespace
)

func main() {
    me := shmem.MyPe()

    major, minor := shmem.InfoGetVersion()

    name := shmem.InfoGetName()

    if me == 0 {
        fmt.Printf(
            "Hello from OpenSHMEM \"%s\" version %d.%d\n",
            name, major, minor)
    }

    shmem.Finalize()
}
