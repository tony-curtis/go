package main

import "bitbucket.org/tony-curtis/go/shmem"

func main() {
    var p shmem.Memory = shmem.Malloc(64)
    shmem.Free(p)

    q := shmem.Calloc(64, 4)
    shmem.Free(q)

    shmem.Finalize()
}
