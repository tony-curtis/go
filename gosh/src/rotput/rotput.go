package main

import (
    "bitbucket.org/tony-curtis/go/shmem"
    "fmt"
)

// symmetric
var dest int = -1
var srce int

func main() {
    me := shmem.MyPe()
    npes := shmem.NPes()

    nextpe := (me + 1) % npes
    srce = nextpe

    shmem.BarrierAll()

    shmem.IntPut(&dest, &srce, 1, nextpe)

    shmem.BarrierAll()

    var msg string
    if dest == me {
        msg = "correct"
    } else {
        msg = fmt.Sprintf("wrong: expected %d", me)
    }

    fmt.Printf("%-4d dest = %-4d: %s\n", me, dest, msg)

    shmem.Finalize()
}
