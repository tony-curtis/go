package main

/*
#include <ucp/api/ucp.h>

#include <pmix.h>
int
go_pmix_get_job_size(pmix_proc_t *me)
{
    pmix_proc_t wc;
    pmix_value_t *vp;
    int res;

    PMIX_PROC_CONSTRUCT(&wc);
    strncpy(wc.nspace, me->nspace, PMIX_MAX_NSLEN + 1);
    wc.rank = PMIX_RANK_WILDCARD;

    PMIx_Get((const pmix_proc_t *) &wc,
             PMIX_JOB_SIZE,
             NULL, 0, &vp);
    res = (int) vp->data.uint32;
    PMIX_VALUE_RELEASE(vp);
    return res;
}
*/
import "C"

import "fmt"
import "os"
import "unsafe"

// -- begin utilities: bail out --------------------------------------

import "log"

func fail(call string, msg string) {
    log.Fatalln(call, " failed: ", msg)
    // not reached
}

// -- end utilities: bail out ----------------------------------------

// -- begin UCX --------------------------------------------------

type ucp_config_ptr     = *(C.ucp_config_t)
type ucs_status         = C.ucs_status_t
type ucp_context        = C.ucp_context_h
type ucp_params         = C.ucp_params_t
type ucp_mem_map_params = C.ucp_mem_map_params_t
type ucp_mem            = C.ucp_mem_h

type ucx_state struct {
    cfg ucp_config_ptr
    ctxt ucp_context

    mh ucp_mem
    heap_base unsafe.Pointer
    heap_size uint64
}

var ucx ucx_state

// couple of debugging utils

func print_cfg(cfg ucp_config_ptr) {
    C.ucp_config_print(cfg, C.stdout, nil, C.UCS_CONFIG_PRINT_CONFIG)
}

func print_context(cx ucp_context) {
    C.ucp_context_print_info(cx, C.stdout)
}

// setup/teardown of UCX

func ucx_init() {
    var s ucs_status

    s = C.ucp_config_read(nil, nil, &ucx.cfg)
    if s != 0 {
        fail("ucp_config_read", C.GoString(C.ucs_status_string(s)))
    }

    pm := ucp_params {
        field_mask: C.UCP_PARAM_FIELD_FEATURES,
        features: C.UCP_FEATURE_RMA,
    }

    s = C.ucp_init(&pm, ucx.cfg, &ucx.ctxt)
    if s != 0 {
        fail("ucp_init", C.GoString(C.ucs_status_string(s)))
    }

    // print_cfg(ucx.cfg)
    // print_context(ucx.ctxt)

    C.ucp_config_release(ucx.cfg)

    mp := ucp_mem_map_params {
        field_mask: C.UCP_MEM_MAP_PARAM_FIELD_LENGTH |
            C.UCP_MEM_MAP_PARAM_FIELD_FLAGS,
        length: 65536,
        flags: C.UCP_MEM_MAP_ALLOCATE,
    }

    s = C.ucp_mem_map(ucx.ctxt, &mp, &ucx.mh)
    if s != 0 {
        fail("ucp_mem_map", C.GoString(C.ucs_status_string(s)))
    }

    var attr C.ucp_mem_attr_t

    s = C.ucp_mem_query(ucx.mh, &attr)
    if s != 0 {
        fail("ucp_mem_query", C.GoString(C.ucs_status_string(s)))
    }

    ucx.heap_base = attr.address
    ucx.heap_size = uint64(attr.length)
}

func ucx_finalize() {
    var s ucs_status

    s = C.ucp_mem_unmap(ucx.ctxt, ucx.mh)
    if s != 0 {
        fail("ucp_mem_unmap", C.GoString(C.ucs_status_string(s)))
    }

    C.ucp_cleanup(ucx.ctxt)
}

// -- end UCX --------------------------------------------------------

// -- begin local state ----------------------------------------------

type proc_state struct {
    rank int
    nranks int
}

var proc proc_state

// -- end local state ------------------------------------------------

// -- begin PMIx -----------------------------------------------------

func pmix_init() {
    var my_proc C.pmix_proc_t

    var ps C.pmix_status_t = C.PMIx_Init(&my_proc, nil, 0)
    if ps != C.PMIX_SUCCESS {
        fail("PMIx_Init", C.GoString(C.PMIx_Error_string(ps)))
    }

    proc.rank = int(my_proc.rank)
    proc.nranks = int(C.go_pmix_get_job_size(&my_proc))
}

func pmix_finalize() {
    var ps C.pmix_status_t = C.PMIx_Finalize(nil, 0)
    if ps != C.PMIX_SUCCESS {
        fail("PMIx_Finalize", C.GoString(C.PMIx_Error_string(ps)))
    }
}

// -- end PMIx -------------------------------------------------------

// -- begin API ------------------------------------------------------

func shmem_finalize() {
    ucx_finalize()
    pmix_finalize()
}

func shmem_init() {
    pmix_init()
    ucx_init()
}

func shmem_my_pe() int {
    return proc.rank
}

func shmem_n_pes() int {
    return proc.nranks
}

// -- end API --------------------------------------------------------

func main() {

    shmem_init()

    me := shmem_my_pe()
    npes := shmem_n_pes()

    host, _ := os.Hostname()

    fmt.Println("Hello from PE ", me, " of ", npes, " on ", host)

    if me == 0 {
        fmt.Println("Heap {", ucx.heap_base, ", ", ucx.heap_size, "}")
    }

    // this may be required in the golang interface
    shmem_finalize()
}
