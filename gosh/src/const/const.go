package main

import (
    "fmt"
    "bitbucket.org/tony-curtis/go/shmem"
)

const WIDTH = -24

func main() {
    fmt.Printf("%*s = %d\n", WIDTH, "BCAST_SYNC_SIZE", shmem.BCAST_SYNC_SIZE)
    fmt.Printf("%*s = %d\n", WIDTH, "BARRIER_SYNC_SIZE", shmem.BARRIER_SYNC_SIZE)
    fmt.Printf("%*s = %d\n", WIDTH, "REDUCE_SYNC_SIZE", shmem.REDUCE_SYNC_SIZE)
    fmt.Printf("%*s = %d\n", WIDTH, "REDUCE_MIN_WRKDATA_SIZE", shmem.REDUCE_MIN_WRKDATA_SIZE)
    fmt.Printf("%*s = %d\n", WIDTH, "SYNC_VALUE", shmem.SYNC_VALUE)

    fmt.Printf("%*s = %d\n", WIDTH, "MAX_NAME_LEN", shmem.MAX_NAME_LEN)

    fmt.Printf("%*s = %d\n", WIDTH, "THREAD_SINGLE", shmem.THREAD_SINGLE)
    fmt.Printf("%*s = %d\n", WIDTH, "THREAD_FUNNELED", shmem.THREAD_FUNNELED)
    fmt.Printf("%*s = %d\n", WIDTH, "THREAD_SERIALIZED", shmem.THREAD_SERIALIZED)
    fmt.Printf("%*s = %d\n", WIDTH, "THREAD_MULTIPLE", shmem.THREAD_MULTIPLE)

    fmt.Printf("%*s = %d\n", WIDTH, "CMP_EQ", shmem.CMP_EQ)
    fmt.Printf("%*s = %d\n", WIDTH, "CMP_NE", shmem.CMP_NE)
    fmt.Printf("%*s = %d\n", WIDTH, "CMP_GT", shmem.CMP_GT)
    fmt.Printf("%*s = %d\n", WIDTH, "CMP_LE", shmem.CMP_LE)
    fmt.Printf("%*s = %d\n", WIDTH, "CMP_LT", shmem.CMP_LT)
    fmt.Printf("%*s = %d\n", WIDTH, "CMP_GE", shmem.CMP_GE)
}
