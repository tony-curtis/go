LD_LIBRARY_PATH=$HOME/opt/pmix/git/lib:$LD_LIBRARY_PATH
LD_LIBRARY_PATH=$HOME/opt/ucx/git/lib:$LD_LIBRARY_PATH
LD_LIBRARY_PATH=$HOME/opt/osss-ucx/git/lib:$LD_LIBRARY_PATH
export LD_LIBRARY_PATH

export CGO_CFLAGS="$(pkg-config --cflags osss-ucx)"

export CGO_LDFLAGS="$(pkg-config --libs-only-L osss-ucx) \
       $(pkg-config --libs-only-l osss-ucx)"
